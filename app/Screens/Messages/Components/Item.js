import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const Item = ({message}) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity
      onPress={() => navigation.navigate('Detail', {name: message.name})}
      style={styles.container}>
      <Image source={message.image} style={styles.image} />
      <View style={styles.column}>
        <Text style={styles.title}>{message.name}</Text>
        <Text style={styles.body}>{message.description}</Text>
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 15,
    paddingBottom: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#e1e1e1',
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 10,
  },
  column: {
    marginLeft: 10,
    flex: 1,
  },
  title: {
    fontWeight: 'bold',
    flex: 1,
    fontSize: 18,
  },
  body: {
    fontSize: 16,
    color: '#b6b6b6',
  },
});
export default Item;
