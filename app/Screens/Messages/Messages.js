import React from 'react';
import {Text, View, StyleSheet, Dimensions} from 'react-native';
import Item from './Components/Item';
import data from './Data';
import {RecyclerListView, DataProvider, LayoutProvider} from 'recyclerlistview';
let dataProvider = new DataProvider((r1, r2) => {
  return r1 !== r2;
});
const {width, height} = Dimensions.get('window');
const Messages = ({params}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Messages</Text>
      <RecyclerListView
        rowRenderer={(type, item) => {
          return <Item message={item} />;
        }}
        dataProvider={dataProvider.cloneWithRows(data)}
        layoutProvider={
          new LayoutProvider(
            (index) => {
              return 0;
            },
            (type, dim) => {
              dim.width = width;
              dim.height = (height * 100) / 667;
            },
          )
        }
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1,
    padding: 20,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 30,
  },
});
export default Messages;
