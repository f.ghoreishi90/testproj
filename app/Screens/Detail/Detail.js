import React from 'react';
import {Text, View, StyleSheet} from 'react-native';

const Detail = ({route}) => {
  const name = route.params.name;

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Name: {name}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: 'white',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
export default Detail;
