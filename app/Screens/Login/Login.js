import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';
const Login = ({params}) => {
  const [isSelectedEmail, setIsSelectedEmail] = useState(false);
  const [isSelectedPass, setIsSelectedPass] = useState(false);
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Image source={require('../../Assets/Images/Logo.png')} />
      <View
        style={[
          styles.textInpView,
          {borderBottomColor: isSelectedEmail ? '#67d4a5' : '#e1e1e1'},
        ]}>
        <Icon name="mail-outline" size={30} color="#a6a6a6" />
        <TextInput
          style={[styles.textInput, {fontWeight: 'normal', fontSize: 17}]}
          onFocus={() => {
            setIsSelectedEmail(true);
          }}
          onBlur={() => {
            setIsSelectedEmail(false);
          }}
          keyboardType={'email-address'}
        />
      </View>
      <View
        style={[
          styles.textInpView,
          {
            marginTop: '5%',
            borderBottomColor: isSelectedPass ? '#67d4a5' : '#e1e1e1',
          },
        ]}>
        <Icon name="lock-closed-outline" size={30} color="#a6a6a6" />
        <TextInput
          style={[styles.textInput]}
          textContentType="password"
          secureTextEntry={true}
          maxLength={8}
          onFocus={() => {
            setIsSelectedPass(true);
          }}
          onBlur={() => {
            setIsSelectedPass(false);
          }}
        />
      </View>
      <View style={styles.row}>
        <Icon name="checkmark-circle-outline" size={30} color="#67d4a5" />
        <Text
          style={{
            fontSize: 16,
          }}>
          {' '}
          Remember me
        </Text>
        <Text style={styles.forgotText}>Forgot password?</Text>
      </View>
      <TouchableOpacity
        style={styles.touch}
        onPress={() => navigation.navigate('Messages')}>
        <Text style={styles.touchText}>LOGIN</Text>
      </TouchableOpacity>
      <View style={[styles.row, {justifyContent: 'center'}]}>
        <Text style={{fontSize: 16}}>Dont have an account?</Text>
        <Text style={{color: '#67d4a5', fontSize: 16}}> Signup Here</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    padding: 20,
  },
  textInpView: {
    width: '100%',
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderBottomColor: '#e1e1e1',
    alignItems: 'center',
    padding: 10,
  },
  textInput: {
    width: '100%',
    fontSize: 30,
    fontWeight: 'bold',
    marginLeft: 5,
  },
  row: {
    flexDirection: 'row',
    marginTop: '5%',
    alignItems: 'center',
    width: '100%',
    // backgroundColor: 'red',
  },
  touch: {
    backgroundColor: '#67d4a5',
    width: '100%',
    marginTop: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 15,
    borderRadius: 5,
    elevation: 12,
  },
  touchText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 18,
  },
  forgotText: {
    color: '#67d4a5',
    textAlign: 'right',
    flex: 1,
    fontSize: 16,
  },
});
export default Login;
